FROM openjdk:11.0.6-jdk

WORKDIR /opt/app

ARG JAR_FILE=target/ctf-1.0.jar

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","app.jar"]