package com.gift.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class CtfFinal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String ctf1Key;
    private String ctf2Key;
    private String ctf3Key;
    private String ctf4Key;
    private String ctf5Key;
    private boolean state;

    public CtfFinal() {}

    public CtfFinal(String ctf1Key, String ctf2Key, String ctf3Key, String ctf4Key, String ctf5Key, boolean state) {
        this.ctf1Key = ctf1Key;
        this.ctf2Key = ctf2Key;
        this.ctf3Key = ctf3Key;
        this.ctf4Key = ctf4Key;
        this.ctf5Key = ctf5Key;
        this.state = state;
    }

    public String getCtf1Key() {
        return ctf1Key;
    }

    public void setCtf1Key(String ctf1Key) {
        this.ctf1Key = ctf1Key;
    }

    public String getCtf2Key() {
        return ctf2Key;
    }

    public void setCtf2Key(String ctf2Key) {
        this.ctf2Key = ctf2Key;
    }

    public String getCtf3Key() {
        return ctf3Key;
    }

    public void setCtf3Key(String ctf3Key) {
        this.ctf3Key = ctf3Key;
    }

    public String getCtf4Key() {
        return ctf4Key;
    }

    public void setCtf4Key(String ctf4Key) {
        this.ctf4Key = ctf4Key;
    }

    public String getCtf5Key() {
        return ctf5Key;
    }

    public void setCtf5Key(String ctf5Key) {
        this.ctf5Key = ctf5Key;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}