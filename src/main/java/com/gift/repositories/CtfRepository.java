package com.gift.repositories;


import com.gift.model.Ctf;
import com.gift.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CtfRepository extends CrudRepository<Ctf, Integer> {

}
