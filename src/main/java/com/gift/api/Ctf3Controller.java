package com.gift.api;

import com.gift.model.Ctf3;
import com.gift.model.Ctf4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class Ctf3Controller {

    @PostMapping("/deep-purple")
    public String ctf3(Ctf3 ctf3) {
        if (ctf3.getFirstKey().equals("Mamkin huligan")) {
            ctf3.setState(true);
        }
        return "deep-purple";
    }

    @RequestMapping(value = "/deep-purple", method = {RequestMethod.OPTIONS})
    public void ctf3(HttpServletResponse response) {
        response.addHeader("PUSSY HEADER", "Mamkin huligan");
        response.addHeader("Save it", "+OOcBLQju6Zu8tVvjcW4hIBjxbxkr3y7oR7WMZFI1fN5adPDsJd1ibG294hGG5NVExzpu4SUZuxaXs41yrKH8gAIeTJCdlSsXbbfBxkzREPCe2E7E2vd49sVawn1OhaMbN6dsp+q0o94FO1Jxmg1C+e602cqut0GMpThG632x8+yrEer6UZTnZ2yiontld0MXuCu9UquDL71npmZOUqBL9vHGUvRy7JEaIjtFPGO+3PfHho+llUG9gu19q4wXVdWQ0JShHxacYPR1ljTw1tzkev6ab00pG8pHBt/bD9bc9eGTPXDS6+Qz7YjdnjNZ6b+qIOkV1EmVNDe2YD1H0909VlDYviq398F0XTa+prnYoHP49s6bxzfLn+1tytr1PqM/Ueo/6qoWhfjLHp9Lt7UQauj2/GU/QdNaI8NNZe6EG4gWu9ucu5XZMkWvRkpVbxvRu5a62uacGLOtty+PxlZ079LTIoD5rA9ROc7hCN8b34vDUddbQut1vrRUFr+mVBCOqykXJH+nxltLQjftbvWJamlI+7TGO/F7O+TKz9g9LBNX6coUBufGavopx04Ro+0uq4MzUAgbdwKq+zlp0fUylEKLePBWPQupahI7imm7OL");
    }

    @GetMapping("/bratishka")
    public String ctf4_1(Ctf4 ctf4, HttpServletResponse response) {
        Cookie cookie = new Cookie("pokushal", "no");
        response.addCookie(cookie);
        return "bratishka";
    }

}
