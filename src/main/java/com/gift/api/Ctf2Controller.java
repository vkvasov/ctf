package com.gift.api;

import com.gift.model.Ctf2;
import com.gift.model.Ctf3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
public class Ctf2Controller {

    @PostMapping("/ctf-kar-mogo-dida")
    public String ctf2(Ctf2 ctf2) {
        if (ctf2.getFirstKey().equals("MamkinHaker v dele )))))")) {
            ctf2.setState(true);
        }
        return "ctf-kar-mogo-dida";
    }

    @GetMapping("/deep-purple")
    public String ctf3(Ctf3 ctf3) {
        return "deep-purple";
    }
}
