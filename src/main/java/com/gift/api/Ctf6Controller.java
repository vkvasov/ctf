package com.gift.api;

import com.gift.model.Ctf6;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Ctf6Controller {


    @GetMapping("/kinki")
    public String ctf6_1(Ctf6 ctf6) {
        return "kinki";
    }

    @PostMapping("/kinki")
    public String ctf6_2(Ctf6 ctf6) {
        if(ctf6.getFirstKey().equals("987654321")){
            ctf6.setState(true);
        }
        return "kinki";
    }


}
