package com.gift.api;

import com.gift.model.Ctf4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

@Controller
public class Ctf4Controller {


    @PostMapping("/bratishka")
    public String ctf4_2(Ctf4 ctf4, HttpServletRequest request) {
        Optional<Cookie> username = Arrays.stream(request.getCookies())
                .filter(cookie -> cookie.getName().equals("pokushal"))
                .findFirst();
        if (username.isPresent() && username.get().getValue().equals("yes")) {
            ctf4.setState(true);
        }
        return "bratishka";
    }


}
