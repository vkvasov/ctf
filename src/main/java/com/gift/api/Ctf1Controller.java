package com.gift.api;

import com.gift.model.Ctf;
import com.gift.model.Ctf2;
import com.gift.repositories.CtfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Ctf1Controller {

    @Autowired
    private final CtfRepository ctfRepository;

    public Ctf1Controller(CtfRepository ctfRepository) {
        this.ctfRepository = ctfRepository;
    }

    @GetMapping("/ctf1")
    public String showSignUpForm(Ctf ctf) {
        return "ctf1";
    }

    @GetMapping("/ctf-kar-mogo-dida")
    public String ctf2(Ctf2 ctf2) {
        return "ctf-kar-mogo-dida";
    }

    @PostMapping("/ctf1")
    public String ctf1(Ctf ctf) {
        if (ctf.getFirstKey().equals("MamkinHacker1")) {
            ctf.setState(true);
        }
        return "ctf1";
    }
}
