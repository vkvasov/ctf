package com.gift.api;

import com.gift.model.Ctf6;
import com.gift.model.Ctf7;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Ctf7Controller {


    @GetMapping("/shpil")
    public String ctf77_1(Ctf7 ctf7) {
        return "shpil";
    }

    @PostMapping("/shpil")
    public String ctf7_2(Ctf7 ctf7) {
        if(ctf7.getFirstKey().equals("22170")){
            ctf7.setState(true);
        }
        return "shpil";
    }


}
