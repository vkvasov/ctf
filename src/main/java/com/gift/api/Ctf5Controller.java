package com.gift.api;

import com.gift.model.Ctf5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Ctf5Controller {

    @GetMapping("/pachka-grechki-i-dve-katroshki")
    public String ctf5_1(Ctf5 ctf5) {
        return "pachka-grechki-i-dve-katroshki";
    }

    @PostMapping("/pachka-grechki-i-dve-katroshki")
    public String ctf5_2(Ctf5 ctf5) {
        if(ctf5.getFirstKey().equals("mamkin olimpiadnik012681012212427")){
            ctf5.setState(true);
        }
        return "pachka-grechki-i-dve-katroshki";
    }


}
