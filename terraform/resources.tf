/*resource "google_compute_network" "our_development_network" {
  name = "devnetwork"
  auto_create_subnetworks = true
}*/

resource "aws_vpc" "env-example" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "aws_test_tag"
  }
}

resource "aws_subnet" "subnet1" {
  cidr_block = "${cidrsubnet(aws_vpc.env-example.cidr_block, 3, 1)}"
  vpc_id = "${aws_vpc.env-example.id}"
  availability_zone = "us-east-2a"
}

resource "aws_subnet" "subnet2" {
  cidr_block = "${cidrsubnet(aws_vpc.env-example.cidr_block, 2, 2)}"
  vpc_id = "${aws_vpc.env-example.id}"
  availability_zone = "us-east-2b"
}

resource "aws_security_group" "subnetsecurity" {
  vpc_id = "${aws_vpc.env-example.id}"

  ingress {
    cidr_blocks = [
      "${aws_vpc.env-example.cidr_block}"
    ]

    from_port = 80
    to_port = 80
    protocol = "tcp"
  }
}